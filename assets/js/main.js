var data = {
    'productData': null
};
var reqParams = null;

var productPage = 1;
var moreProductCounter = 0;
var totalNum = 0;
var filtersPopulated = false;
var productsPopulated = false;
var loadedFirstData = false;


var filterPopulation = function (data) {
    filtersPopulated = true;
    var filters = data.filters;
    var filtersLength = filters.length;
    for (let i = 0; i < filtersLength; i++) {
        let filter = filters[i];
        let template = document.createElement('div');
        template.className = 'group';
        template.id = 'group' + i;
        let expander = document.createElement('div');
        let expanderBck = document.createElement('span');
        expander.className = 'expander';
        expander.innerText = filter.label;
        expander.appendChild(expanderBck);
        template.appendChild(expander);
        let items = document.createElement('div');
        items.className = 'items';


        var values = filter.values;
        var valuesLength = values.length;

        for (let j = 0; j < valuesLength; j++) {
            let item = document.createElement('label');
            item.className = 'item';
            item.innerText = values[j].name;
            let count = document.createElement('span');
            count.innerText = '(' + (values[j].count >= 40 ? 40 : values[j].count) + ')';
            let input = document.createElement('input');
            input.type = 'checkbox';
            input.name = filter.name;
            input.value = values[j].id;
            let checkmark = document.createElement('span');
            checkmark.className = 'checkmark';

            item.appendChild(count);
            item.appendChild(input);
            item.appendChild(checkmark);
            items.appendChild(item);
            template.appendChild(items);
        }
        document.getElementById('filters').appendChild(template);


    }
};

var filterManipulation = function () {
    var itemElms = document.getElementsByTagName('input');

    for (let i = 0; i < itemElms.length; i++) {

        itemElms[i].addEventListener('click', function (e) {
            reqParams = null;
            resetProductLoadingCounters();
            e.stopPropagation();
            var children = this.children;

            for (let j = 0; j < itemElms.length; j++) {

                if (itemElms[j].nodeName.toLowerCase() == 'input' && itemElms[j].checked == true) {
                    // checkmarks[i].checked = false;
                    if (reqParams !== null && reqParams.indexOf(itemElms[j].name) != -1) {

                        var namePosition = reqParams.indexOf(itemElms[j].name);
                        var nameLength = itemElms[j].name.length;
                        var posLen = (namePosition + nameLength);
                        var commaPosition = reqParams.indexOf(',', posLen);
                        var reqParams = [reqParams.slice(0, commaPosition + 1), (itemElms[j].value + ','), reqParams.slice(commaPosition + 1)].join('');


                    } else {
                        if (reqParams !== null) {
                            reqParams = reqParams + '&' + itemElms[j].name + '=' + itemElms[j].value + ',';
                        } else {
                            reqParams = '?' + itemElms[j].name + '=' + itemElms[j].value + ',';
                        }


                    }


                } else {

                }

            }
            productReq(url, reqParams);

        });

    }

}

var clearFilters = function () {
    reqParams = null;
    resetProductLoadingCounters();
    var checkmarks = document.getElementById('filters').getElementsByTagName('input');
    for (let i = 0; i < checkmarks.length; i++) {

        if (checkmarks[i].nodeName.toLowerCase() == 'input') {
            checkmarks[i].checked = false;
        }
    }
    productReq(url, reqParams);
}
var productPopulation = function (data) {

    productsPopulated = true;


    if (data.products.length % 10 === 0) {
        totalNum = (moreProductCounter + 1) * 10;
        if (totalNum - 1 >= (data.products.length - 10)) {
            toggleMore();
        }
    } else {

        if (((moreProductCounter + 1) * 10) < data.products.length) {
            totalNum = (moreProductCounter + 1) * 10;
            if (totalNum - 1 >= (data.products.length - (data.products.length % 10))) {

                toggleMore();

            }
        } else {

            totalNum = (moreProductCounter * 10) + (data.products.length % 10);
            if (totalNum - 1 >= (data.products.length - (data.products.length % 10))) {

                toggleMore();

            }
        }
    }

    if (data.products.length >= totalNum) {
        for (let i = (moreProductCounter * 10); i < totalNum/*data.products.length*/; i++) {
            var node = document.getElementsByClassName('product-item')[0].cloneNode(true);
            var description = document.getElementsByClassName('details-cnt')[0].cloneNode(true);
            var product = data.products[i];
            var dealNum = product.shops.count;
            node.getElementsByTagName('h2')[0].innerHTML = product.title;
            description.getElementsByClassName('details-body')[0].getElementsByTagName('p')[0].innerHTML = product.description;
            for (let i = 0; i < product.productImage[0].links.length; i++) {
                if (product.productImage[0].links[i].rel === 'image-original') {
                    var a = node.getElementsByClassName('img-hld')[0].children[0].src = product.productImage[0].links[i].href;


                }

            }

            for (let i = 0; i < dealNum; i++) {
                var deal = document.getElementsByClassName('deal')[0].cloneNode(true);
                var dealData = product.shops.shopitems[i];
                node.getElementsByClassName('deals')[0].appendChild(deal);
                var price = deal.getElementsByClassName('price')[0];
                var merchant = deal.getElementsByClassName('merchant')[0];
                var link = deal.getElementsByTagName('a')[0];
                price.innerText = dealData.costs.price.price + ' ';
                merchant.innerText = dealData.name;
                var linkData = dealData.links;
                for (let i = 0; i < linkData.length; i++) {
                    if (linkData[i].rel === 'clickout') {
                        link.href = linkData[i].href;
                    }

                }
                var currency = document.createElement('span');
                currency.className = 'currency';
                currency.innerText = dealData.costs.currencySymbol;
                price.appendChild(currency);
                //currency.innerHTML = dealData.costs.currencySymbol;


            }


            node.getElementsByClassName('price')[0].innerHTML = product;

            document.getElementById('product-list').appendChild(node);
            document.getElementById('product-list').appendChild(description);
        }

        resetProductLoadingCounters();
    } else {

        //moreProductCounter = 0;
        /* productPage += 1;
         reqParams = reqParams + ('&page=' + productPage);
         productReq(url, reqParams);*/
        //TODO send parameter request;
        /* productPopulation();*/
    }
};

var setProductData = function (response) {
    data.productData = response;

    removeProducts('.product-item');
    removeProducts('.details-cnt');
    if (!filtersPopulated) {
        filterPopulation(data.productData);
        filterManipulation();
        toggleMore();
    }
    if (!loadedFirstData) {
        toggleLoader();
        loadedFirstData = true;
    }

    productPopulation(data.productData);
    attachClicks();
    showFiltersAndProducts();


}

var resetProductLoadingCounters = function () {
    totalNum = 0;
    moreProductCounter = 0;
}

const url = 'http://holistic-page.herokuapp.com/';

var productReq = function (url, params) {
    var script = document.createElement('script');
    script.id = 'holProdScript'
    script.type = 'text/javascript';
    document.body.appendChild(script);
    script.src = params === null ? url : (url + params);
    script.onload = function () {
        document.body.removeChild(this);
    }
}


document.addEventListener("DOMContentLoaded", function (event) {
    productReq(url, reqParams);

    document.getElementById('clear-btn').addEventListener('click', function () {
        clearFilters();
    });
    document.getElementById('more').addEventListener('click', function () {
        moreProductCounter += 1;
        productPopulation(data.productData);
    });


});

